package i_introduction._1_Java_To_Kotlin_Converter

import util.TODO

/**
 * 任务1
 * 使用Kotlin重写JavaCode1.task1
 * 在IEDA中，可以仅仅使用“复制-粘贴”代码，并且同意自动转化为Kotlin代码，但是仅仅适用于该任务！
 */
fun todoTask1(collection: Collection<Int>): Nothing = TODO(
        """
        Task 1.
        Rewrite JavaCode1.task1 in Kotlin.
        In IntelliJ IDEA, you can just copy-paste the code and agree to automatically convert it to Kotlin,
        but only for this task!
    """,
        references = { JavaCode1().task1(collection) }
)

fun task1_(collection: Collection<Int>): String {
    var str: String = "{"
    for (value in collection) {
        str += value
        str += ", "
    }
    str = str.removeRange((str.length - 2)..(str.length - 1))
    return str + "}"
}

fun task1(collection: Collection<Int>): String {

//    todoTask1(collection)

    val sb = StringBuilder()
    sb.append("{")
    val iterator = collection.iterator()
    while (iterator.hasNext()) {
        val element = iterator.next()
        sb.append(element)
        if (iterator.hasNext()) {
            sb.append(", ")
        }
    }
    sb.append("}")
    return sb.toString()
}
