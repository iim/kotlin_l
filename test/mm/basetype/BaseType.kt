package mm.basetype

import com.sun.xml.internal.fastinfoset.util.StringArray
import mm.utils.p
import mm.utils.pd
import mm.utils.pdm
import mm.utils.pl
import org.junit.Test

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2017/6/14
 * Email：ibelieve1210@163.com
 */
class BaseType {

    /**
     * 显式转换（Kotlin中没有隐式转换）
     */
    @Test fun test_explicitConversions() {

        val a: Int = 1

        //此处a不能隐式的从Int类型转换成Long类型，只能通过显式方式转换，但前提是a不能为空的Int类型（Int?）
        val b: Long? = a.toLong()
        pl("a is $a, and b is $b.")

        val c: Int = 1
        val d = c + 1L //此处d为Long类型
        pl("d's type is ${d.javaClass}")

        val e = d + 1.1f
        pl("e's type is ${e.javaClass}")

        val f = d + 1.1
        pl("f's type is ${f.javaClass}")

        val g = c + 1.1f
        pl("g's type is ${g.javaClass}")

        val h = c + 1.1
        pl("h's type is ${h.javaClass}")
    }

    /**
     * 将字符显示转化成Int
     */
    @Test fun test_Char2Int() {
        var c: Char = 'c'
        if (c !in '0'..'9') {
            pl("out of range")
        } else {
            var x = c.toInt()
            pl("x is $x, and it's type is " + x.javaClass)
        }
    }

    /**
     * 数组
     */
    @Test fun test_array() {
        //创建数组
        val array1: Array<Int> = arrayOf(1, 2, 3)
        pl(array1[1])

        pd()

        // 创建定长数组
        val array2 = arrayOfNulls<Int>(10)
        pl(array2.get(1))

        pd()

        //创建一个数组[0,1,4,9,16]
        val array3 = Array(5, { i -> i * i })
        for (i in array3) {
            p("$i, ")
        }

        pdm("其他数组的表达形式")
        val array4: StringArray = StringArray()
        array4.add(null)
        array4.add("null")
        array4.add("hello")
        p("${array4.get(0)}, ")
        p("${array4.get(1)}, ")
        p("${array4.get(2)}, ")
        pl("${array4.get(3)}")

        pd()
        val array5: IntArray = intArrayOf(0, 1, 2)
        for (i in array5) {
            p("$i, ")
        }
    }


    @Test fun test_string() {
        val string: String = "String\n"
        for (char in string) {
            p(char + ", ")
        }

        pd()
        val s1: String = """可以
换行的$string"""
        pl(s1)

        pd()
        val s2: String = """这 里面
  |*含有 空格
  和 换行 """.trimMargin("|*")
        pl(s2)

        pd()
        val s3: String = " 前面 中间 后面 都有空格 "
        pl(s3.trim())
        pl(s3.trimStart())
        pl(s3.trimEnd())

        pd()
        pl("${'$'}is money unit")
    }


}