package mm.utils

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2017/6/16
 * Email：ibelieve1210@163.com
 */

fun pd() {
    println("\n*********************************************************************\n")
}

fun pdm(msg: Any?) {
    println("\n********************************** $msg ***********************************\n")
}

fun pl(msg: Any?) {
    println(msg)
}

fun p(msg: Any?) {
    print(msg)
}