package mm.function

import mm.utils.pl
import org.junit.Test

/**
 * Descriptions：函数以及λ表达式
 * <p>
 * Author：ChenME
 * Date：2017/6/16
 * Email：ibelieve1210@163.com
 */
class Fun_Lambda {
    /**  **************************************** 求平方的函数 ************************************************  */


    fun square(len: Int): Int {
        return len * len
    }

    val square1: (Int) -> Int = { len: Int -> len * len }
    val square2: (Int) -> Int = { len -> len * len }
    val square3: (Int) -> Int = { it * it }

    @Test fun test_lambda() {
        //正常用法
        pl(square(3))

        //使用λ表达式
        pl(square1.invoke(3))

        //使用λ表达式，在函数体中省略参数的类型
        pl(square2.invoke(3))

        //使用λ表达式，单个参数的时候，使用it代替参数
        pl(square3.invoke(3))

        // 匿名函数直接调用，这里没有指明返回类型，但是可以推导出返回值类型为 Int
        val result: Int = { len: Int -> len * len }(3)
        pl(result)

    }

    /**  **************************************** 求平方的函数 ************************************************ */

    val month = "(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)"

    fun getPattern() = "(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)"
    @Test fun test() {
        pl("JAN".matches(getPattern().toRegex()))
    }
}